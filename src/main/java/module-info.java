module com.swt.calculator {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.kordamp.ikonli.javafx;
    requires org.kordamp.bootstrapfx.core;

    opens com.swt.calculator to javafx.fxml;
    exports com.swt.calculator;
}